# FoundryVTT Status Icon Counters

This is the repository of the status icon counter addon for FoundryVTT.

## Usage

**Status Icon Counters** allows setting and displaying a counter on any token's status effects.

![Status Icon Counters](sample.png "Status Icon Counters")

The module has several options. The **font size** and **color** should be self-explanatory.

The **mouse rebind** option changes the click behavior as follows:
- Left click on a status icon (in the token HUD) increases the stack count by 1. If it wasn't active before, it is added.
- Right click on a status icon decreases the stack count by 1. If it reaches 0, the effect is removed. If it is already inactive, the overlay effect is toggled instead.
- Shift + left click on a status icon always toggles the overlay effect.

The **number key rebind** behaves as follows:
- Pressing any number key while hovering an effect sets the stack count to that number. If it wasn't active before, it is added.
- Pressing 0 removes the effect.

Holding the Alt key during any bind modifies the effect's duration instead of the counter value.

----

Using **Ctrl + Left click** on an active status effect opens its advanced counter configuration. The following options are available:
- The **amount** allows you to set an arbitrary stack count. This is useful if your system uses large values that would take a lot of clicks to enter.
- The **duration** can be set to display the effect's remaining duration in **rounds** or **turns**. It is possible to display both an amount and a duration at the same time.
- The **modify duration** toggle is used to change the keybinds (both mouse and number keys) to affect the duration instead of the counter's value. The **Alt** key can then be used to change the value instead of the duration.
- The **multiply effect** setting enables the modification of any underlying active effect changes with the stack count. For example, if an effect would decrease your health by 4, its value will be increased to 8 for 2 stacks, 12 for 3 stacks, etc. For effects that are already multipliers, the change is modified additively (e.g. 0.8 for 1 stack becomes 0.6 for 2 stacks, not 0.64).

Saving the configuration as default will reapply these settings (except for the value) every time an effect for that status is created. Otherwise, the settings are only applied to this specific effect and will be reset when it is removed.

----

System developers can define their own counter types and defaults for each effect. Read the documentation below to find out how.

## Development

Everything you (should) need is contained in the [counter API](API.md). If it's not, feel free to open a feature suggestion on this repository.

Accessing a counter is as simple as calling the `effect.statusCounter` getter on any `ActiveEffect` instance. Additional APIs (e.g. for defining custom counter classes) can be accessed using `game.modules.get("statuscounter")?.api`.

Alternatively, your module can define a dependency in its module.json and `include ../../modules/statuscounter/module/counter.js`.

Here are some common examples of things you might want to do with the status counters:

### Modify a counter

Calling `setValue` on any counter changes its value:

```javascript
await effect.statusCounter.setValue(effect.statusCounter.displayValue * 2);
```
For built-in types, setting a value equal or below 0 will remove or disable the effect. Custom types may changes this behavior to allow negative values.

The counter's `set(value)`, `increment()` and `decrement()` may be used to adjust the value *or duration*, based on what the keybinds would change. The optional `alt` flag inverts this behavior. For macros, it is recommended to explicitly specify what you intend to change (`setValue(value)` or `setDuration(value)`).

### Listen for counter changes

You can hook onto `updateActiveEffect` in order to receive events for counter changes.

```javascript
Hooks.on("updateActiveEffect", function(effect, changes) {
    const counterConfig = foundry.utils.getProperty(changes, "flags.statuscounter.config");
    const counterValue = foundry.utils.getProperty(changes, "flags.statuscounter.value");
    if (counterConfig || counterValue) { /* Do something */ }
});
```

### Add custom counter types

The API also allows you to create custom counter types that can have their own logic and font.

To add a type, use:

```javascript
const counterApi = game.modules.get("statuscounter")?.api;
if (!counterApi) return;

class TheChosenOne extends counterApi.StatusCounter {
    get displayValue() { return 1; }
    setValue(value) {
        if (value !== 1) {
            console.error('not the chosen one');
            return this._deleteParent();
        }

        return this.parent.update({
            [this.dataSource]: value,
            "flags.statuscounter.visible": true,
        });
    }
}

counterApi.addCounterType("mymodule.chosen1", TheChosenOne);
```

The `addCounterType` method may optionally receive an array of status ids for which your type will be used by default. Otherwise, the user will have to select the type in the counter's configuration.

### Change the data source

Writing the counter value to a different effect property does not require a custom type. You can simply set the `dataSource` when creating the counter:

```javascript
Hooks.on("preCreateActiveEffect", effect => {
    effect.updateSource({
        "flags.statuscounter.config.dataSource": "flags.mymodule.stacks",
        "flags.mymodule.stacks": effect.getFlag("statuscounter", "value") ?? 1,
    });
});
```
