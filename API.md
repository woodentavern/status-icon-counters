## Members

<dl>
<dt><a href="#label">label</a> ⇒ <code>string</code></dt>
<dd></dd>
<dt><a href="#visible">visible</a> ⇒ <code>boolean</code></dt>
<dd></dd>
<dt><a href="#displayValue">displayValue</a> ⇒ <code>number</code></dt>
<dd></dd>
<dt><a href="#displayDuration">displayDuration</a> ⇒ <code>number</code></dt>
<dd></dd>
</dl>

## Functions

<dl>
<dt><a href="#allowType">allowType(parent)</a> ⇒ <code>boolean</code></dt>
<dd><p>Checks if this counter type can be used for the given effect.</p>
</dd>
<dt><a href="#create">create(document, data)</a></dt>
<dd><p>Applies data for a new counter to the given document&#39;s source.</p>
</dd>
<dt><a href="#createFont">createFont(size)</a> ⇒ <code>PIXI.TextStyle</code></dt>
<dd><p>Creates a new canvas font for rendering the counter&#39;s value.</p>
</dd>
<dt><a href="#createDurationFont">createDurationFont(size)</a> ⇒ <code>PIXI.TextStyle</code></dt>
<dd><p>Creates a new canvas font for rendering the counter&#39;s duration.</p>
</dd>
<dt><a href="#setValue">setValue(value)</a> ⇒ <code>Promise</code></dt>
<dd><p>Updates the value of the counter&#39;s data source.</p>
</dd>
<dt><a href="#increment">increment([alt])</a> ⇒ <code>Promise</code></dt>
<dd><p>Increments the value or duration of the counter by 1.</p>
</dd>
<dt><a href="#decrement">decrement([alt])</a> ⇒ <code>Promise</code></dt>
<dd><p>Decrements the value or duration of the counter by 1.</p>
</dd>
<dt><a href="#set">set(value, [alt])</a> ⇒ <code>Promise</code></dt>
<dd><p>Updates the value or duration of the counter.</p>
</dd>
<dt><a href="#setDuration">setDuration(value)</a> ⇒ <code>Promise</code></dt>
<dd><p>Updates the duration of the counter&#39;s effect.</p>
</dd>
<dt><a href="#configure">configure()</a> ⇒ <code>Promise</code></dt>
<dd><p>Opens the configuration for this counter.</p>
</dd>
<dt><a href="#_isNegative">_isNegative(value)</a> ⇒ <code>boolean</code></dt>
<dd><p>Checks if the counter&#39;s value would become 0 when setting the given value.</p>
</dd>
<dt><a href="#_isOver">_isOver(duration)</a> ⇒ <code>boolean</code></dt>
<dd><p>Checks if the effect&#39;s duration would become 0 when setting the given duration.</p>
</dd>
<dt><a href="#_deleteParent">_deleteParent()</a> ⇒ <code>Promise</code></dt>
<dd><p>Removes the parent effect from its actor.</p>
</dd>
<dt><a href="#_multiplyDelta">_multiplyDelta(delta)</a> ⇒ <code>*</code></dt>
<dd><p>Multiplies the given delta value with the counter&#39;s value.</p>
</dd>
</dl>

<a name="label"></a>

## label ⇒ <code>string</code>
**Kind**: global variable  
**Returns**: <code>string</code> - A human readable name for this counter type.  
<a name="visible"></a>

## visible ⇒ <code>boolean</code>
**Kind**: global variable  
**Returns**: <code>boolean</code> - True if the value counter should be displayed, false otherwise.  
<a name="displayValue"></a>

## displayValue ⇒ <code>number</code>
**Kind**: global variable  
**Returns**: <code>number</code> - The counter value to display.  
<a name="displayDuration"></a>

## displayDuration ⇒ <code>number</code>
**Kind**: global variable  
**Returns**: <code>number</code> - The remaining duration to display.  
<a name="allowType"></a>

## allowType(parent) ⇒ <code>boolean</code>
Checks if this counter type can be used for the given effect.

**Kind**: global function  
**Returns**: <code>boolean</code> - True if this counter type can be used, false otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| parent | <code>Document</code> | The effect to check. |

<a name="create"></a>

## create(document, data)
Applies data for a new counter to the given document's source.

**Kind**: global function  

| Param | Type | Description |
| --- | --- | --- |
| document | <code>Document</code> | The document that is being created. |
| data | <code>object</code> | The data to create the counter with. |

<a name="createFont"></a>

## createFont(size) ⇒ <code>PIXI.TextStyle</code>
Creates a new canvas font for rendering the counter's value.

**Kind**: global function  
**Returns**: <code>PIXI.TextStyle</code> - The font to use.  

| Param | Type | Description |
| --- | --- | --- |
| size | <code>number</code> | The target size of the font. |

<a name="createDurationFont"></a>

## createDurationFont(size) ⇒ <code>PIXI.TextStyle</code>
Creates a new canvas font for rendering the counter's duration.

**Kind**: global function  
**Returns**: <code>PIXI.TextStyle</code> - The font to use.  

| Param | Type | Description |
| --- | --- | --- |
| size | <code>number</code> | The target size of the font. |

<a name="setValue"></a>

## setValue(value) ⇒ <code>Promise</code>
Updates the value of the counter's data source.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the effect update.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>number</code> | The value to set. |

<a name="increment"></a>

## increment([alt]) ⇒ <code>Promise</code>
Increments the value or duration of the counter by 1.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the effect update.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [alt] | <code>boolean</code> | <code>false</code> | Toggle the modification of the value or the duration. |

<a name="decrement"></a>

## decrement([alt]) ⇒ <code>Promise</code>
Decrements the value or duration of the counter by 1.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the effect update.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [alt] | <code>boolean</code> | <code>false</code> | Toggle the modification of the value or the duration. |

<a name="set"></a>

## set(value, [alt]) ⇒ <code>Promise</code>
Updates the value or duration of the counter.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the effect update.  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| value | <code>number</code> |  | The value to set. |
| [alt] | <code>boolean</code> | <code>false</code> | Toggle the modification of the value or the duration. |

<a name="setDuration"></a>

## setDuration(value) ⇒ <code>Promise</code>
Updates the duration of the counter's effect.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the effect update.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>number</code> | The duration to set. |

<a name="configure"></a>

## configure() ⇒ <code>Promise</code>
Opens the configuration for this counter.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the configuration rendering.  
<a name="_isNegative"></a>

## \_isNegative(value) ⇒ <code>boolean</code>
Checks if the counter's value would become 0 when setting the given value.

**Kind**: global function  
**Returns**: <code>boolean</code> - True if the new value would be 0 or lower, false otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>number</code> | The value to check. |

<a name="_isOver"></a>

## \_isOver(duration) ⇒ <code>boolean</code>
Checks if the effect's duration would become 0 when setting the given duration.

**Kind**: global function  
**Returns**: <code>boolean</code> - True if the new duration would be 0 or lower, false otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| duration | <code>number</code> | The duration to check. |

<a name="_deleteParent"></a>

## \_deleteParent() ⇒ <code>Promise</code>
Removes the parent effect from its actor.

**Kind**: global function  
**Returns**: <code>Promise</code> - A promise representing the deletion.  
<a name="_multiplyDelta"></a>

## \_multiplyDelta(delta) ⇒ <code>\*</code>
Multiplies the given delta value with the counter's value.

**Kind**: global function  
**Returns**: <code>\*</code> - The multiplied delta value.  

| Param | Type | Description |
| --- | --- | --- |
| delta | <code>\*</code> | The delta value provided by FoundryVTT. |

